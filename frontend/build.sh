rm -rf spa
cd ../../src/frontend
npm install
npm run build
cd dist
cp ../../../teletutoring-cloud/frontend/nginx.conf ./nginx.conf
cp ../../../teletutoring-cloud/frontend/fullchain.pem ./fullchain.pem
cp ../../../teletutoring-cloud/frontend/privkey.pem ./privkey.pem
docker build . -f ../../../teletutoring-cloud/frontend/Dockerfile -t teletutoring_frontend:0.2
cd ../../../teletutoring-cloud/frontend/
