#!/bin/bash

BACKEND_HOST=`ip route get 1 | head -1 | cut -d' ' -f7`
BACKEND_PORT=3001

docker run -it -p4200:80 -eBACKEND_HOST=$BACKEND_HOST -eBACKEND_PORT=$BACKEND_PORT teletutoring_frontend:0.2